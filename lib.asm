section .text

%define SYSEXIT 60
%define NULLTERMINATED_SYMBOL 0
%define SYSWRITE 1
%define STDOUT 1
%define STDERR 2
%define NEWLINE_SYMBOL 0xA
%define ACII_NUMBERS_CODE 0x30
%define MINUS_CHAR "-"
%define BIT_DIVIDER 10
%define SPACE_SYMBOL 0x20
%define TAB_SYMBOL 0x9


%include "lib.inc"

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSEXIT
    xor rdi, rdi
    syscall


; принимает указатель на строку, возвращает её длину. rcx - счетчик
string_length:
    xor rcx, rcx
    .cycle:
    cmp byte[rdi+rcx], NULLTERMINATED_SYMBOL
    je  .end
    inc rcx
    jmp .cycle
    .end:
    mov rax, rcx
    ret


; вывод строки, первый аргумент — указатель на неё
; установим rsi, rdx, rdi и rax параметры, перед вызовом sys_write
; rsi — адрес начала, rdx — кол-ство символов
; rdi — поток ВВ, rax — номер системной команды
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, SYSWRITE
    mov rdi, STDOUT
    syscall
    ret



; аргумент — символ, который нужно вывести в stdout
; rdi - печатаемый символ
print_char:
    push rdi
    mov rsi, rsp
    mov rdi, STDOUT
    mov rdx, 1
    mov rax, SYSWRITE
    syscall
    pop rdi
    ret



; переход на новую строку
print_newline:
    mov rdi, NEWLINE_SYMBOL	
    call print_char
    ret


; вывод беззнакового 8-байтового числа в десятичной СС
; методом деления на 10 (0x0A). После div, в rdx падает остаток от деления.
; add rdx, „0“ устанавливает символ rdx до цифрового ASCII-кода
print_uint:
    mov rax, rdi
    mov r8, NEWLINE_SYMBOL
    push NULLTERMINATED_SYMBOL
    .splitting_by_digits:
    xor rdx, rdx
    div r8
    add rdx, ACII_NUMBERS_CODE
    push rdx
    test rax, rax
    jnz .splitting_by_digits
    .print:
    pop rdi
    test rdi, rdi
    jz .exit
    call print_char
    jmp .print
    .exit:
    ret



; вывод знакового 8-байтного числа в десятичной СС
print_int:
    test rdi, rdi
    js .neg
    call print_uint
    ret
    .neg:
    push rdi
    mov rdi, MINUS_CHAR
    call print_char
    pop rdi
    neg rdi
    call print_uint
    ret



; принимает два указателя на строки(rax, rdx), сравнивает их посимвольно
; если они равны, то в rax возвращает 1, иначе 0
string_equals:
    xor rcx, rcx
    .loop:
   	mov al, byte[rdi+rcx]
    	mov dl, byte[rsi+rcx]
    	cmp al, dl
    	jne .not_equal
   	inc rcx
    	cmp al, NULLTERMINATED_SYMBOL
    	je .equal
    	jmp .loop
.not_equal:
    xor rax, rax
    ret
     .equal:
    mov rax, 1
    ret


; прочитать с потока ввода один символ
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push rax
    mov rsi, rsp
    syscall
    pop rax
    ret



; прочитать с потока ввода следующее слово, пропустив перед пробельные символы
; возвращает: в rax - слово, в rdx — длина слова
read_word:
    push rdi
    push rsi
    mov r8, rdi
    mov r9, rsi
    xor r10, r10
    .cycle_before_letters:
    call read_char
    cmp rax, NULLTERMINATED_SYMBOL
    je .end
    cmp rax, 0x20
    je .cycle_before_letters
    cmp rax, 0x9
    je .cycle_before_letters
    cmp rax, 0xA
    je .cycle_before_letters
    jmp .write
    .reading_cycle:
    call read_char
    cmp rax, NULLTERMINATED_SYMBOL
    je .end
    cmp rax, SPACE_SYMBOL
    je .end
    cmp rax, TAB_SYMBOL
    je .end
    cmp rax, NEWLINE_SYMBOL
    je .end
    jmp .write
    .write:
    mov byte[r8+r10], al
    inc r10
    dec r9
    test r9, r9
    jnz .reading_cycle
    jmp .check_last_letter
    .check_last_letter:
    test rax, rax
    jnz .error
    jmp .end
    .error:
    xor rax, rax
    ret
    .end:
    mov byte[r8+r10], 0
    mov rax, r8
    dec rcx
    mov rdx, r10
    pop rsi
    pop rdi
    ret



; прочитать из буфера в памяти беззнаковое число
; вернуть в rax — число, rdx — длину числа
parse_uint:
    xor rcx, rcx
    xor rdx, rdx
    xor rax, rax
    mov r8, BIT_DIVIDER
    .reading_symbols:
    mov dl, byte[rdi+rcx]
    cmp rdx, ACII_NUMBERS_CODE
    jl .finish_reading
    cmp rdx, ACII_NUMBERS_CODE + 9
    jg .finish_reading
    sub rdx, ACII_NUMBERS_CODE
    jmp .shift_and_plus
    .shift_and_plus:
    push rdx
    mul r8
    pop rdx
    add rax, rdx
    inc rcx
    jmp .reading_symbols
    .finish_reading:
    mov rdx, rcx
    ret 



; прочитать из буфера в памяти знаковое число
; вернуть в rax — число, rdx — длину числа
parse_int:
    cmp byte[rdi], MINUS_CHAR
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret


; принимает указатель на строку и указатель на место в памяти
; после выполнения по второму адресу лежит копия первой строки
string_copy:
    push rdi
    call string_length
    pop rdi
    cmp rax , rdx
    jg .fail
    .cycle:
    mov r8b, byte[rdi]
    mov byte[rsi], r8b
    inc rdi
    inc rsi
    test r8b, r8b
    jnz .cycle 
    ret
    .fail:
    mov rax, 0
    ret


print_exc:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rax, SYSWRITE
    mov rdi, STDERR
    syscall
    ret

