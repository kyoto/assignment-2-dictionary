%define buffSize 255

section .data
too_much_symbols_exception: db "Input stream too long", 0
not_found_exception: db "Your word not found", 0

section .bss
inBuffer: resb buffSize

section .text
%include "colon.inc"
%include "words.inc"
%include "main.inc"

_start:
    mov rdi, inBuffer
    mov rsi, buffSize
    call read_word
    test rax, rax
    jz .too_long
    mov rsi, pnt 
    call find_word
    test rax, rax
    jz .not_found

    add rax, 8
    mov rdi, rax
    call string_length
    add rdi, rax
    inc rdi
    call print_string
    call exit

.too_long:
    mov rdi, too_much_symbols_exception
    call print_exc
    call exit

.not_found:
    mov rdi, not_found_exception
    call print_exc
    call exit
